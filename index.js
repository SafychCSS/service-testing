const checkStatus = status => (status === 'in' ? 1 : -1);
function testService(arr) {
    if (!arr.length) return true;
    if (arr.length === 1) return false;

    const guests = {};
    for (let i = 0; i < arr.length; i += 1) {
        const [name, status] = arr[i];
        if (!guests[name]) {
            guests[name] = 0;
        }
        guests[name] += checkStatus(status);
        if (guests[name] > 1 || guests[name] < 0) return false;
    }
    const numbers = new Set(Object.values(guests).map(value => value));
    if (numbers.size > 1 || !numbers.has(0)) return false;
    return true;
}

module.exports = testService;
